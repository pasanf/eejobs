<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    $user_type = ["employee", "employer", "admin"];
    return [
        'user_type' => $user_type[rand(0, 2)],
        'username' => $faker->userName,
        'login_password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'other_name' => $faker->name,
        'nic' => $faker->shuffle('1234567890'),
        'phone_number' => $faker->phoneNumber,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
        'is_notification_enabled' => $faker->boolean(50),
        'is_notification_enabled_for_promotion' => $faker->boolean(50),
        'last_login' => now(),
        'firebase_token' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'account_expiry_date' => $faker->dateTimeBetween('+0 days', '+2 years'),
        'first_warning_date' => $faker->dateTimeBetween('+0 days', '+2 years'),
        'second_warning_date' => $faker->dateTimeBetween('+0 days', '+2 years'),
        'is_verified' => $faker->boolean(50),
        'is_account_disabled' => $faker->boolean(50),
        'account_disabled_on' => $faker->boolean(50),
        'last_payment_date' => $faker->boolean(50),
    ];
});
