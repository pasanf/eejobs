<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->integer('id',true);
            $table->string('user_type',30)->nullable();
            $table->string('username',50)->nullable();
            $table->text('login_password')->nullable();
            $table->string('first_name',80);
            $table->string('last_name',80);
            $table->string('other_name',100)->nullable();
            $table->string('nic',20)->nullable();
            $table->string('phone_number',30)->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->text('password');
            $table->boolean('is_notification_enabled')->default(0);
            $table->boolean('is_notification_enabled_for_promotion')->default(0);
            $table->timestamp('last_login')->nullable();
            $table->text('firebase_token')->nullable();
            $table->date('account_expiry_date')->nullable();
            $table->date('first_warning_date')->nullable();
            $table->date('second_warning_date')->nullable();
            $table->boolean('is_verified')->default(0);
            $table->boolean('is_account_disabled')->default(0);
            $table->boolean('account_disabled_on')->default(0);
            $table->boolean('last_payment_date')->default(0);
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
