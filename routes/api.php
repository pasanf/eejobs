<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('mans', 'ManController');

//Route::middleware(['auth:api'])->group(function () {
//    Route::resource('users', 'UserController');
//});

Route::post('create','UserController@store')->name('user.create');

Route::post('debug','DebugController@index')->name('debug.index');
