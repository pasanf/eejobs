<section class="section section-lg">
    <div class="container">
        <div class="row justify-content-center text-center mb-lg">
            <div class="col-lg-8">
                <h2 class="display-3">Top Freelance Categories</h2>
                <p class="lead text-muted">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo
                    ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-lg-3  mb-lg-0">
                <ul class="list-unstyled">
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-settings-gear-65"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Computer Software <span>(95)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-html5"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">CAD Design <span>(23)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-satisfied"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Plumbers <span>(45)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-settings-gear-65"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Computer Software <span>(95)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-html5"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">CAD Design <span>(23)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-satisfied"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Plumbers <span>(45)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-settings-gear-65"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Computer Software <span>(95)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-html5"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">CAD Design <span>(23)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-satisfied"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Plumbers <span>(45)</span></h6>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-md-6 col-lg-3  mb-lg-0">
                <ul class="list-unstyled">
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-settings-gear-65"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Computer Software <span>(95)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-html5"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">CAD Design <span>(23)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-satisfied"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Plumbers <span>(45)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-settings-gear-65"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Computer Software <span>(95)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-html5"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">CAD Design <span>(23)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-satisfied"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Plumbers <span>(45)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-settings-gear-65"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Computer Software <span>(95)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-html5"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">CAD Design <span>(23)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-satisfied"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Plumbers <span>(45)</span></h6>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-md-6 col-lg-3  mb-lg-0">
                <ul class="list-unstyled">
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-settings-gear-65"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Computer Software <span>(95)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-html5"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">CAD Design <span>(23)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-satisfied"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Plumbers <span>(45)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-settings-gear-65"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Computer Software <span>(95)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-html5"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">CAD Design <span>(23)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-satisfied"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Plumbers <span>(45)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-settings-gear-65"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Computer Software <span>(95)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-html5"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">CAD Design <span>(23)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-satisfied"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Plumbers <span>(45)</span></h6>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-md-6 col-lg-3  mb-lg-0">
                <ul class="list-unstyled">
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-settings-gear-65"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Computer Software <span>(95)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-html5"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">CAD Design <span>(23)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-satisfied"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Plumbers <span>(45)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-settings-gear-65"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Computer Software <span>(95)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-html5"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">CAD Design <span>(23)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-satisfied"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Plumbers <span>(45)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-settings-gear-65"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Computer Software <span>(95)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-html5"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">CAD Design <span>(23)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-satisfied"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Plumbers <span>(45)</span></h6>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
