<section class="section section-lg" style="padding-top: 0px">
    <div class="container">
        <div class="row justify-content-center text-center mb-lg">
            <div class="col-lg-8">
                <h2 class="display-3">Find perfect freelancers for your projects</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-lg-3 mb-lg-0">
                <div class="px-4">
                    <img src="../assets/img/theme/team-1-800x800.jpg" class="rounded-circle img-center img-fluid shadow shadow-lg--hover" style="width: 200px;">
                    <div class="pt-4 text-center">
                        <h5 class="title">
                            <span class="d-block mb-1">Ryan Tompson</span>
                            <small class="h6 text-muted">Web Developer</small>
                        </h5>
                        <div class="mt-3">
                            <a href="#" class="btn btn-warning btn-icon-only rounded-circle">
                                <i class="fa fa-phone"></i>
                            </a>
                            <a href="#" class="btn btn-primary btn-icon-only rounded-circle">
                                <i class="fa fa-comment"></i>
                            </a>
                            <a href="#" class="btn btn-success btn-icon-only rounded-circle">
                                <i class="fa fa-dribbble"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 mb-5 mb-lg-0">
                <div class="px-4">
                    <img src="../assets/img/theme/team-2-800x800.jpg" class="rounded-circle img-center img-fluid shadow shadow-lg--hover" style="width: 200px;">
                    <div class="pt-4 text-center">
                        <h5 class="title">
                            <span class="d-block mb-1">Romina Hadid</span>
                            <small class="h6 text-muted">Marketing Strategist</small>
                        </h5>
                        <div class="mt-3">
                            <a href="#" class="btn btn-warning btn-icon-only rounded-circle">
                                <i class="fa fa-phone"></i>
                            </a>
                            <a href="#" class="btn btn-primary btn-icon-only rounded-circle">
                                <i class="fa fa-comment"></i>
                            </a>
                            <a href="#" class="btn btn-success btn-icon-only rounded-circle">
                                <i class="fa fa-dribbble"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 mb-5 mb-lg-0">
                <div class="px-4">
                    <img src="../assets/img/theme/team-3-800x800.jpg" class="rounded-circle img-center img-fluid shadow shadow-lg--hover" style="width: 200px;">
                    <div class="pt-4 text-center">
                        <h5 class="title">
                            <span class="d-block mb-1">Alexander Smith</span>
                            <small class="h6 text-muted">UI/UX Designer</small>
                        </h5>
                        <div class="mt-3">
                            <a href="#" class="btn btn-warning btn-icon-only rounded-circle">
                                <i class="fa fa-phone"></i>
                            </a>
                            <a href="#" class="btn btn-primary btn-icon-only rounded-circle">
                                <i class="fa fa-comment"></i>
                            </a>
                            <a href="#" class="btn btn-success btn-icon-only rounded-circle">
                                <i class="fa fa-dribbble"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 mb-5 mb-lg-0">
                <div class="px-4">
                    <img src="../assets/img/theme/team-4-800x800.jpg" class="rounded-circle img-center img-fluid shadow shadow-lg--hover" style="width: 200px;">
                    <div class="pt-4 text-center">
                        <h5 class="title">
                            <span class="d-block mb-1">John Doe</span>
                            <small class="h6 text-muted">Founder and CEO</small>
                        </h5>
                        <div class="mt-3">
                            <a href="#" class="btn btn-warning btn-icon-only rounded-circle">
                                <i class="fa fa-phone"></i>
                            </a>
                            <a href="#" class="btn btn-primary btn-icon-only rounded-circle">
                                <i class="fa fa-comment"></i>
                            </a>
                            <a href="#" class="btn btn-success btn-icon-only rounded-circle">
                                <i class="fa fa-dribbble"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
