<section class="section section-lg" style="padding-top: 0px">
    {{--<div class="container">--}}
    <div class="row justify-content-center text-center mb-lg" style="margin: 0px !important;">
        <div class="col-lg-8">
            <h2 class="display-3">Browse numerous freelance jobs online</h2>
        </div>
    </div>
    <div class="row" style="padding: 25px">
        <div class="col-md-6 col-lg-4">
            <div class="card shadow shadow-lg--hover mt-5">
                <div class="card-body">
                    <div class="d-flex px-1">
                        <div class="pl-4">
                            <h5 class="title text-primary">Need a Plumber</h5>
                            <p><i class="fa fa-map-marker"></i> Kandy</p>
                            <p>The Arctic Ocean freezes every winter and much of the sea-ice then thaws every summer, </p>
                            <p class="mt-3 mb-0 text-sm">
                                <span><i class="fa fa-calendar"></i> December 13, 2018</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4">
            <div class="card shadow shadow-lg--hover mt-5">
                <div class="card-body">
                    <div class="d-flex px-1">
                        <div class="pl-4">
                            <h5 class="title text-danger">Need a Plumber</h5>
                            <p><i class="fa fa-map-marker"></i> Kandy</p>
                            <p>The Arctic Ocean freezes every winter and much of the sea-ice then thaws every summer, </p>
                            <p class="mt-3 mb-0 text-sm">
                                <span><i class="fa fa-calendar"></i> December 13, 2018</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4">
            <div class="card shadow shadow-lg--hover mt-5">
                <div class="card-body">
                    <div class="d-flex px-1">
                        <div class="pl-4">
                            <h5 class="title text-primary">Need a Plumber</h5>
                            <p><i class="fa fa-map-marker"></i> Kandy</p>
                            <p>The Arctic Ocean freezes every winter and much of the sea-ice then thaws every summer, </p>
                            <p class="mt-3 mb-0 text-sm">
                                <span><i class="fa fa-calendar"></i> December 13, 2018</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4">
            <div class="card shadow shadow-lg--hover mt-5">
                <div class="card-body">
                    <div class="d-flex px-1">
                        <div class="pl-4">
                            <h5 class="title text-danger">Need a Plumber</h5>
                            <p><i class="fa fa-map-marker"></i> Kandy</p>
                            <p>The Arctic Ocean freezes every winter and much of the sea-ice then thaws every summer, </p>
                            <p class="mt-3 mb-0 text-sm">
                                <span><i class="fa fa-calendar"></i> December 13, 2018</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4">
            <div class="card shadow shadow-lg--hover mt-5">
                <div class="card-body">
                    <div class="d-flex px-1">
                        <div class="pl-4">
                            <h5 class="title text-danger">Need a Plumber</h5>
                            <p><i class="fa fa-map-marker"></i> Kandy</p>
                            <p>The Arctic Ocean freezes every winter and much of the sea-ice then thaws every summer, </p>
                            <p class="mt-3 mb-0 text-sm">
                                <span><i class="fa fa-calendar"></i> December 13, 2018</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4">
            <div class="card shadow shadow-lg--hover mt-5">
                <div class="card-body">
                    <div class="d-flex px-1">
                        <div class="pl-4">
                            <h5 class="title text-danger">Need a Plumber</h5>
                            <p><i class="fa fa-map-marker"></i> Kandy</p>
                            <p>The Arctic Ocean freezes every winter and much of the sea-ice then thaws every summer, </p>
                            <p class="mt-3 mb-0 text-sm">
                                <span><i class="fa fa-calendar"></i> December 13, 2018</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    {{--</div>--}}
</section>
