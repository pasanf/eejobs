import React from "react";
import PropTypes from "prop-types";
// react plugin for creating charts
import ChartistGraph from "react-chartist";
// @material-ui/core
import withStyles from "@material-ui/core/styles/withStyles";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Store from "@material-ui/icons/Store";
import Warning from "@material-ui/icons/Warning";
import DateRange from "@material-ui/icons/DateRange";
import LocalOffer from "@material-ui/icons/LocalOffer";
import Update from "@material-ui/icons/Update";
import ArrowUpward from "@material-ui/icons/ArrowUpward";
import AccessTime from "@material-ui/icons/AccessTime";
import Accessibility from "@material-ui/icons/Accessibility";
import BugReport from "@material-ui/icons/BugReport";
import Code from "@material-ui/icons/Code";
import Cloud from "@material-ui/icons/Cloud";
// core components
import GridItem from "../../js/components/Grid/GridItem.jsx";
import GridContainer from "../../js/components/Grid/GridContainer.jsx";
import Table from "../../js/components/Table/Table.jsx";
import Tasks from "../../js/components/Tasks/Tasks.jsx";
import CustomTabs from "../../js/components/CustomTabs/CustomTabs.jsx";
import Danger from "../../js/components/Typography/Danger.jsx";
import Card from "../../js/components/Card/Card.jsx";
import CardHeader from "../../js/components/Card/CardHeader.jsx";
import CardIcon from "../../js/components/Card/CardIcon.jsx";
import CardBody from "../../js/components/Card/CardBody.jsx";
import CardFooter from "../../js/components/Card/CardFooter.jsx";

import { bugs, website, server } from "../../variables/general.jsx";

import {
  dailySalesChart,
  emailsSubscriptionChart,
  completedTasksChart
} from "../../variables/charts.jsx";

import dashboardStyle from "../../assets/jss/material-dashboard-react/views/dashboardStyle.jsx";

class Dashboard extends React.Component {
  //   constructor(props){
  //       super(props);
  //       this.props = props;
  //       this.state = {value: 0};
  //   }
  // handleChange (event, value){
  //   this.setState({ value });
  // };

  render() {
    const { classes } = this.props;
    return (
      <div>
          <GridContainer>
              <GridItem xs={12} sm={6} md={3}>
                  <Card>
                      <CardHeader color="warning" stats icon>
                          <CardIcon color="warning">
                              <Store />
                          </CardIcon>
                          <p className={classes.cardCategory}>Used Space</p>
                          <h3 className={classes.cardTitle}>
                              49/50
                          </h3>
                      </CardHeader>
                      <CardFooter stats>
                          <div className={classes.stats}>
                              <Danger>
                                  <Warning />
                              </Danger>
                              <a href="#pablo" onClick={e => e.preventDefault()}>
                                  Get more space
                              </a>
                          </div>
                      </CardFooter>
                  </Card>
              </GridItem>
              <GridItem xs={12} sm={6} md={3}>
                  <Card>
                      <CardHeader color="success" stats icon>
                          <CardIcon color="success">
                              <Store />
                          </CardIcon>
                          <p className={classes.cardCategory}>Revenue</p>
                          <h3 className={classes.cardTitle}>$3445</h3>
                      </CardHeader>
                      <CardFooter stats>
                          <div className={classes.stats}>
                              <DateRange />
                              Last 24 Hours
                          </div>
                      </CardFooter>
                  </Card>
              </GridItem>
              <GridItem xs={12} sm={6} md={3}>
                  <Card>
                      <CardHeader color="danger" stats icon>
                          <CardIcon color="danger">
                              <Accessibility />
                          </CardIcon>
                          <p className={classes.cardCategory}>Fixed Issues</p>
                          <h3 className={classes.cardTitle}>75</h3>
                      </CardHeader>
                      <CardFooter stats>
                          <div className={classes.stats}>
                              <LocalOffer />
                              Tracked from Github
                          </div>
                      </CardFooter>
                  </Card>
              </GridItem>
              <GridItem xs={12} sm={6} md={3}>
                  <Card>
                      <CardHeader color="info" stats icon>
                          <CardIcon color="info">
                              <Accessibility />
                          </CardIcon>
                          <p className={classes.cardCategory}>Followers</p>
                          <h3 className={classes.cardTitle}>+245</h3>
                      </CardHeader>
                      <CardFooter stats>
                          <div className={classes.stats}>
                              <Update />
                              Just Updated
                          </div>
                      </CardFooter>
                  </Card>
              </GridItem>
          </GridContainer>

      </div>
    );
  }
}

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(Dashboard);
