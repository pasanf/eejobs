import * as React from "react";
import GridContainer from "../../js/components/Grid/GridContainer";
import GridItem from "../../js/components/Grid/GridItem";
import CardHeader from "../../js/components/Card/CardHeader";
import CardBody from "../../js/components/Card/CardBody";
import Card from "../../js/components/Card/Card";
import PropTypes from "prop-types";
import CustomInput from "../../js/components/CustomInput/CustomInput";
import {Component} from "react";
import Table from "../../js/components/Table/Table";
import TableHead from "@material-ui/core/TableHead/TableHead";
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import TableBody from "@material-ui/core/TableBody/TableBody";
import {
    dangerColor, defaultFont, grayColor,
    infoColor,
    primaryColor, roseColor,
    successColor,
    warningColor
} from "../../assets/jss/material-dashboard-react";

const styles = {
    labelRoot: {
        ...defaultFont,
        color: "#AAAAAA !important",
        fontWeight: "400",
        fontSize: "14px",
        lineHeight: "1.42857"
    },
    cardCategoryWhite: {
        color: "rgba(255,255,255,.62)",
        margin: "0",
        fontSize: "14px",
        marginTop: "0",
        marginBottom: "0"
    },
    cardTitleWhite: {
        color: "#FFFFFF",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none"
    }, warningTableHeader: {
        color: warningColor
    },
    primaryTableHeader: {
        color: primaryColor
    },
    dangerTableHeader: {
        color: dangerColor
    },
    successTableHeader: {
        color: successColor
    },
    infoTableHeader: {
        color: infoColor
    },
    roseTableHeader: {
        color: roseColor
    },
    grayTableHeader: {
        color: grayColor
    },
    table: {
        marginBottom: "0",
        width: "100%",
        maxWidth: "100%",
        backgroundColor: "transparent",
        borderSpacing: "0",
        borderCollapse: "collapse"
    },
    tableHeadCell: {
        color: "inherit",
        ...defaultFont,
        fontSize: "1em"
    },
    tableCell: {
        ...defaultFont,
        lineHeight: "1.42857143",
        padding: "12px 8px",
        verticalAlign: "middle"
    },
    tableResponsive: {
        width: "100%",
        // marginTop: theme.spacing.unit * 3,
        marginTop: "10px",
        overflowX: "auto"
    }
};

class AdJob extends React.Component {
    constructor(props) {
        super();
        this.state = {
            firstName: '',
            lastName: '',
            user: []
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.renderMans = this.renderMans.bind(this);
        // this.getMans();
    }

    handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
        console.log('onChange', this.state.firstName);
        // console.log('onChange', this.state.lastName);
    }

    handleSubmit(e) {
        e.preventDefault();
        let dd = {
            "id": this.state.user.length + 1,
            "first_name": this.state.firstName,
            "last_name": this.state.lastName,

        };

        let user = this.state.user;
        user[this.state.user.length] = dd;

        this.setState({user});
        axios
            .post('/api/mans', {
                first_name: this.state.firstName,
                last_name: this.state.lastName
            })
            .then(response => {
                console.log('from handle submit', response);
                // set state
                this.setState({
                    user: [response.data, ...this.state.user]
                });
                // then clear the value of textarea
                this.setState({
                    firstName: '',
                    lastName: '',
                });
            });
    }

    render() {
        return (
            <div>
                <GridContainer>
                    <GridItem xs={4} sm={4} md={4}>
                        <Card>
                            <CardHeader color="primary">
                                <h4 className={styles.cardTitleWhite}>Profile</h4>
                            </CardHeader>
                            <CardBody>
                                <GridContainer>
                                    <form onSubmit={this.handleSubmit}>
                                        <GridItem xs={12} sm={12} md={6}>
                                            {/*<CustomInput*/}
                                                {/*onChange={this.handleChange}*/}
                                                {/*value={this.state.firstName}*/}
                                                {/*className="form-control"*/}
                                                {/*placeholder="First Name"*/}
                                                {/*name="firstName"*/}
                                                {/*required*/}
                                            {/*/>*/}
                                            {/*<CustomInput*/}
                                                {/*labelText="First Name"*/}
                                                {/*id="first_name"*/}
                                                {/*name="firstName"*/}
                                                {/*value={this.state.firstName}*/}
                                                {/*onChange={this.handleChange}*/}
                                                {/*formControlProps={{*/}
                                                    {/*fullWidth: true*/}
                                                {/*}}*/}
                                            {/*/>*/}
                                        </GridItem>

                                        <GridItem xs={12} sm={12} md={6}>
                                            <input
                                                value={this.state.lastName}
                                                onChange={this.handleChange}
                                                className="form-control"
                                                placeholder="Last Name"
                                                name="lastName"
                                                required
                                            />
                                        </GridItem>
                                        <button type="submit" className="btn btn-primary">
                                            Create
                                        </button>
                                    </form>
                                </GridContainer>

                                {this.renderMans()}
                            </CardBody>
                        </Card>
                    </GridItem>
                    <GridItem xs={8} sm={8} md={8}>
                        <Card>
                            <CardHeader color="primary">
                                <h4 className={styles.cardTitleWhite}>Simple Table</h4>
                                <p className={styles.cardCategoryWhite}>
                                    Here is a subtitle for this table
                                </p>
                            </CardHeader>
                            <CardBody>
                                <Table
                                    tableHeaderColor="primary"
                                    tableHead={["First", "last"]}
                                    tableData={this.state.user}
                                />
                            </CardBody>
                        </Card>
                    </GridItem>
                </GridContainer>
            </div>

        );
    };


    renderMans() {
        return this.state.user.map(man => (
            <div key={man.id} className="media">
                <div className="media-body">
                    <p>{man.first_name}</p>
                </div>
            </div>
        ));
    }

    getMans() {
        axios.get('/api/mans').then((
            response
            ) => {
                console.log(response.data.mans);
                this.setState({
                    user: [...response.data.mans]
                })
            }
            // this.setState({
            //     user: [...response.data.mans]
            // })
        );
    }

    // lifecycle method
    componentWillMount() {
        this.getMans();
    }
}

export default AdJob;
