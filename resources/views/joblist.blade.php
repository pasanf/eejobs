@extends('layouts.front-layout')
@section('content')
    {{--@include('layouts.partials.menu')--}}
    <div class="row">
        <div class="col-lg-8">
            <div class="section" style="padding-bottom: 0;padding-top: 0">
                <div class="col-md-12">
                    <div class="card shadow shadow-lg--hover mt-5">
                        <div class="card-body">
                            <div class="d-flex px-3">
                                <div>
                                    <div class="icon icon-shape bg-gradient-warning rounded-circle text-white">
                                        <i class="ni ni-satisfied"></i>
                                    </div>
                                </div>
                                <div class="pl-4">
                                    <h5 class="title text-warning">Need a Plumber</h5>
                                    <p>The Arctic Ocean freezes every winter and much of the sea-ice then thaws every summer, and that process will continue whatever.</p>
                                    <a href="#" class="text-warning">more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section" style="padding-bottom: 0;padding-top: 0">
                <div class="col-md-12">
                    <div class="card shadow shadow-lg--hover mt-5">
                        <div class="card-body">
                            <div class="d-flex px-3">
                                <div>
                                    <div class="icon icon-shape bg-gradient-danger rounded-circle text-white">
                                        <i class="ni ni-satisfied"></i>
                                    </div>
                                </div>
                                <div class="pl-4">
                                    <h5 class="title text-danger">Repairman Needed</h5>
                                    <p>The Arctic Ocean freezes every winter and much of the sea-ice then thaws every summer, and that process will continue whatever.</p>
                                    <a href="#" class="text-danger">more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section" style="padding-bottom: 0;padding-top: 0">
                <div class="col-md-12">
                    <div class="card shadow shadow-lg--hover mt-5">
                        <div class="card-body">
                            <div class="d-flex px-3">
                                <div>
                                    <div class="icon icon-shape bg-gradient-pink rounded-circle text-white">
                                        <i class="ni ni-satisfied"></i>
                                    </div>
                                </div>
                                <div class="pl-4">
                                    <h5 class="title text-pink">Need to Install Windows</h5>
                                    <p>The Arctic Ocean freezes every winter and much of the sea-ice then thaws every summer, and that process will continue whatever.</p>
                                    <a href="#" class="text-pink">more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section" style="padding-bottom: 0;padding-top: 0">
                <div class="col-md-12">
                    <div class="card shadow shadow-lg--hover mt-5">
                        <div class="card-body">
                            <div class="d-flex px-3">
                                <div>
                                    <div class="icon icon-shape bg-gradient-success rounded-circle text-white">
                                        <i class="ni ni-satisfied"></i>
                                    </div>
                                </div>
                                <div class="pl-4">
                                    <h5 class="title text-success">Awesome Support</h5>
                                    <p>The Arctic Ocean freezes every winter and much of the sea-ice then thaws every summer, and that process will continue whatever.</p>
                                    <a href="#" class="text-success">more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section" style="padding-bottom: 0;padding-top: 0">
                <div class="col-md-12">
                    <div class="card shadow shadow-lg--hover mt-5">
                        <div class="card-body">
                            <div class="d-flex px-3">
                                <div>
                                    <div class="icon icon-shape bg-gradient-success rounded-circle text-white">
                                        <i class="ni ni-satisfied"></i>
                                    </div>
                                </div>
                                <div class="pl-4">
                                    <h5 class="title text-success">Awesome Support</h5>
                                    <p>The Arctic Ocean freezes every winter and much of the sea-ice then thaws every summer, and that process will continue whatever.</p>
                                    <a href="#" class="text-success">Learn more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="col-md-12 col-lg-12  mb-lg-0">
                <ul class="list-unstyled">
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-settings-gear-65"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Computer Software <span>(95)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-html5"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">CAD Design <span>(23)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-satisfied"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Plumbers <span>(45)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-settings-gear-65"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Computer Software <span>(95)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-html5"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">CAD Design <span>(23)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-satisfied"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Plumbers <span>(45)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-settings-gear-65"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Computer Software <span>(95)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-html5"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">CAD Design <span>(23)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-settings-gear-65"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Computer Software <span>(95)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-html5"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">CAD Design <span>(23)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-satisfied"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Plumbers <span>(45)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-settings-gear-65"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Computer Software <span>(95)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-html5"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">CAD Design <span>(23)</span></h6>
                            </div>
                        </div>
                    </li>
                    <li class="py-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <div class="badge badge-circle badge-success mr-3">
                                    <i class="ni ni-satisfied"></i>
                                </div>
                            </div>
                            <div>
                                <h6 class="mb-0">Plumbers <span>(45)</span></h6>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endsection
