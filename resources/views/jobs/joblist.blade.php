<div class="section" style="padding-bottom: 0;padding-top: 0">
    <div class="col-md-12">
        <div class="card shadow shadow-lg--hover mt-5">
            <div class="card-body">
                <div class="d-flex px-3">
                    <div>
                        <div class="icon icon-shape bg-gradient-success rounded-circle text-white">
                            <i class="ni ni-satisfied"></i>
                        </div>
                    </div>
                    <div class="pl-4">
                        <h5 class="title text-success">Awesome Support</h5>
                        <p>The Arctic Ocean freezes every winter and much of the sea-ice then thaws every summer, and that process will continue whatever.</p>
                        <a href="#" class="text-success">Learn more</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
