import Dashboard from "@material-ui/icons/Dashboard";

import DashboardPage from "../views/Dashboard/Dashboard.jsx";
import AdJob from "../views/advJobs/AdJob";

const dashboardRoutes = [
  {
    path: "/dashboard",
    sidebarName: "Dashboard",
    navbarName: "Material Dashboard",
    icon: Dashboard,
    component: DashboardPage
  },{
    path: "/advjobs",
    sidebarName: "Jobs",
    navbarName: "Advert Jobs",
    icon: Dashboard,
    component: AdJob
  },
  // { redirect: true, path: "/", to: "/dashboard", navbarName: "Redirect" }
];

export default dashboardRoutes;
