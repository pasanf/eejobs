import Dashboard from "../views/layouts/Dashboard/Dashboard.jsx";

const indexRoutes = [{ path: "/", component: Dashboard }];

export default indexRoutes;
