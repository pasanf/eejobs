<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        $status = 200;
        $message = "success";
        $data = [
            'status' => $status,
            'message' => $message,
            'data' => $users,
        ];
        return response($data, $status);
    }

    public function store(UserRequest $request)
    {
        $request= (object)json_decode($request->getContent(), true);
        $username = $request->phone_number;
        $first_name = $request->first_name;
        $last_name = $request->last_name;
        $other_name = $request->other_name;
        $nic = $request->nic;
        $phone_number = $request->phone_number;
        $email = $request->email;
        $password = $request->password;
        $user_type = 'sss';

        try {
            $user = new User();
            $user->username = $username;
            $user->first_name = $first_name;
            $user->last_name = $last_name;
            $user->other_name = $other_name;
            $user->nic = $nic;
            $user->phone_number = $phone_number;
            $user->email = $email;
            $user->password = Hash::make($password);
            $user->user_type = $user_type;
            $user->save();
        }catch (\PDOException $PDOException){
            return json_encode(null);

        }

        return json_encode($user);


    }
}
